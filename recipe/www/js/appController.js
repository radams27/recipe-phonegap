define([
	'backbone',
	'marionette',
	'homeView',
    'recipeView',
    'transitionRegion'

	

],
	function (
		Backbone,
		Marionette,
		HomeView,
        RecipeView,
        TransitionRegion
		

		) {

		var AppController = Marionette.Controller.extend({
		    container:null,
		    appModel:null,
            currentRecipe:null,
			initialize: function (options) {
				_.bindAll(this);
				this.app = options.app;
				this.appModel = options.model;
				this.currentRecipe = "";
			},



			showView: function (viewID, id,cat)
			{

			    var view = null;
			    
           
				switch (viewID) {

				    case '':
				        
				        //this.photoEditSection = new PhotoEditSection({ model: this.photoEditSectionModel });
				         //this.container.show(this.photoEditSection);
				        view = new HomeView({ model: this.appModel });
				         
				        this.app.mainRegion.transitionToView(view, true);
				         
				        //view = new StartView();
                       
				        break;
				    case 'recipe':
                        
				        //this.photoEditSection = new PhotoEditSection({ model: this.photoEditSectionModel });
				        //this.container.show(this.photoEditSection);
				        var list = this.appModel.get('categories')
				        var that = this;
				        var currentModel = this.appModel.get('categories').findWhere({ category: cat })
				        var that = this;
				        _.each(currentModel.get('recipes').models, function (model) {
				            if (model.get('id') == id) {
				                that.currentRecipe = model;
				            }
				            
				        });
				        
				        var model = currentModel.get('recipes').findWhere({ id: id });
				       
				        view = new RecipeView({ model: this.currentRecipe });

				        this.app.mainRegion.transitionToView(view, false);

				        //view = new StartView();

				        break;



					
				}
				if (view == null ) {
					//Utils.log('Start page or views not set up');
				} else {
				  // this.photoEditSection.pageRegion.transitionToView(view);
				    //this.photoEditSection.pageRegion.show(view);

                    // Analytics Pagetracking
				    //Analytics.trackPage(viewID);
				}
			}

		});
		return AppController;

	});