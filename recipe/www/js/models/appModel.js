define([
        'backbone',
        'marionette',
        'appEvents'

],
    function (
        Backbone,
        Marionette,
        CategoryTemplate,
        AppEvents


        ) {
        var AppModel = Backbone.Model.extend({
            recipes: null,
            count:null,
            
            initialize:function(){
                _.bindAll(this);

                

                if (localStorage.getItem('categories') != null) {

                    var Recipe = Backbone.Model.extend({
                        defaults: {
                            title: '',
                            id: '',
                            category: ''

                        },
                    })



                    var Category = Backbone.Model.extend({
                        defaults: {
                            category: "",
                            recipes: ""

                        },

                        initialize: function () {
                            if (this.get('recipes').length > 0) {
                                var recipes = this.get('recipes').map(function (recipe) {
                                    return new Recipe(recipe);
                                });
                                //var Recipes = Backbone.Collection();
                                this.set("recipes", new Backbone.Collection(recipes));
                                console.log('recipes', recipes)

                            }

                        }
                    })
                    var arr = JSON.parse(localStorage.getItem('categories'));
                    var categories = _.map(arr, function (category) {
                        return new Category(category);

                    });


                    //this.recipes = new Backbone.Collection();
                    var categories = new Backbone.Collection(categories);
                    //this.set({ recipes: recipes });
                    this.set({ categories: categories });
                    this.count = 0;

                    console.log('test', this.get('categories'))
                    //this.set("comments", new CommentsCollection(commentModels));



                } else {
                    var categories = new Backbone.Collection();
                    this.set({ categories: categories });
                }

                

            },

            addCategory: function (cat)
            {
                var category = new Backbone.Model();
                var recipe = new Backbone.Model();
                recipe.set({ category: 'default', id: 0, title:'', recipe:'' })
                var col = new Backbone.Collection();
                col.add(recipe)
                category.set({ category: cat, recipes: col });
                var that = this;
                if (!_.contains(this.get('categories'), category.get('category'))) {
                    that.get('categories').add(category);
                    console.log("category added with category: " + category.get('category'));
                    localStorage.setItem('categories', JSON.stringify(this.get('categories')))
                }
                else {
                    console.log("model already exists")
                }
                
            },

            addRecipe: function (title,recipe, categoryName)
            {
                //var recipe = this.get('recipes').findWhere({ category: category });
                //recipe.set({ recipe: recipe });
                this.count++;
                var category = this.get('categories').findWhere({ category: categoryName });
                
                //category.set({title:title, recipe:recipe})
                //this.get('recipes').add({title:title, recipe:recipe,category:category})
                if(category.get('recipes') == undefined)
                {
                    var recipes = new Backbone.Collection();
                    category.set({recipes:recipes});
                    category.get('recipes').add({ id: this.count, category: title, recipe: recipe, cat: categoryName })
                    localStorage.setItem('categories', JSON.stringify(this.get('categories')));
                }else{
                    category.get('recipes').add({ id: this.count, category: title, recipe: recipe, cat: categoryName })
                    localStorage.setItem('categories', JSON.stringify(this.get('categories')));
                }
               
                console.log("cat: " , this.get('categories'));
            },

            deleteCategory: function (category)
            {
                var category = this.get('categories').findWhere({ category: category });
                this.get('categories').remove(category);
                localStorage.setItem('categories', JSON.stringify(this.get('categories')));

            },
            deleteRecipe: function (recipe) {
               // var category = this.get('categories').findWhere({ category: category });
                //this.get('categories').remove(category);
                var recipeToRemove;
                var that = this;
                _.each(this.get('categories').models, function (category) {
                    recipeToRemove = category.get('recipes').where({ category: recipe });
                    category.get('recipes').remove(recipeToRemove)
                });
                console.log('recipe to re', recipeToRemove);
                localStorage.setItem('categories', JSON.stringify(this.get('categories')));

            }
        });

        return AppModel;
    });
