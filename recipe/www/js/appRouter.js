define([
    'backbone',
    'marionette'
    
],
    function (
        Backbone,
        Marionette,
        Utils
        ) {
        
        var AppRouter = Marionette.AppRouter.extend({
            
            controller: null,
            
            initialize: function (options) {
               // Utils.log('[AppRouter initialize]');
                this.controller = options.controller;
            },

            routes: {
                '': 'home',
                'recipe/:cat/:id':'recipe'
            
            },

            home: function () {
                this.controller.showView('', null);
            },
            recipe: function (cat,id) {
                console.log('route model',id,cat)
                this.controller.showView('recipe', id,cat);
            }

           
        });
        return AppRouter;

    });