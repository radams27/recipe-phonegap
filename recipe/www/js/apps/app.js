define([
		'backbone',
		'marionette',
		'underscore',
		'jquery',
		'tweenlite',
		'appRouter',
		'appController',
		'homeView',
        'appModel',
        'transitionRegion',
        'fastClick'
        
        

		//'facebook'
	],
	function(
		Backbone,
		Marionette,
		_,
		$,
		TweenLite,
		AppRouter,
		AppController,
		HomeView,
        AppModel,
        TransitionRegion,
        FastClick

		//Facebook,
	) {  

		function initialize() {   

			//window.App = new Marionette.Application();
			var App = new Marionette.Application();

			 App.addRegions({
			     mainRegion:{
			         selector:'#container',
			         regionType: TransitionRegion
			         }
			 });

		
			
			 var appModel = new AppModel();
			var appController = new AppController({ app:App, model:appModel});
			var appRouter = new AppRouter({ controller: appController });
			
			App.on('start', function(options) {
				
			    FastClick.attach(document.body);
				Backbone.history.start(); // Great time to do this

			
			});

            var options = {};
			App.start(options);

			// Vars ___________________________________________________________________________
			//Utils.is_ie = $('html').hasClass('ie');

			
		}

		return {
			initialize: initialize
		};
	});