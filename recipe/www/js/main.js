require.config({
    //baseUrl: window.appinfo.apppath + 'assets/js',
    paths: {
        backbone: 'lib/backbone',
        underscore: 'lib/underscore-1.4.4-development',
        marionette: 'lib/backbone.marionette',
        facebook: 'lib/facebook',
        fbsdk: '//connect.facebook.net/en_US/all',
        jquery: 'lib/jquery',
        homeView:'views/homeView',
        text: 'lib/text',
           
        tweenlite: 'lib/TweenLite-1.11.1.min',
        cssPlugin: 'lib/CSSPlugin-1.11.0.min',
        easePack: 'lib/EasePack-1.11.0.min',
        spinner: 'lib/spin.min',
        appEvents: 'AppEvents',
        categoryView:'views/categoryView',
        categoriesView: 'views/categoriesView',
        categoryItemView: 'views/categoryItemView',
        categoryPopupView: 'views/categoryPopupView',
        createRecipeView: 'views/createRecipeView',
        categoryCompositeView: 'views/categoryCompositeView',
        recipeView: 'views/recipeView',
        transitionRegion: 'views/TransitionRegion',
        fastClick: 'lib/fastclick',
        iScroll:'lib/iscroll-lite',
        //recipeItemView:'views/recipeItemView',
        //recipesCollectionView:'views/recipesCollectionView',
        appModel:'models/appModel'

    },
    shim: {
        'backbone': {
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        },
        'marionette': {
            deps : ['jquery', 'underscore', 'backbone'],
            exports : 'Marionette'
        },
        'underscore': {
            exports: '_'
        },
    	
        'spinner': {
            exports: 'Spinner'
        },
        'tweenlite': {
            deps: ['jquery', 'easePack', 'cssPlugin'],
            exports: 'TweenLite'
        },
        // If the library does not support AMD
            'iScroll': {
                exports: 'iScroll'
            },
        
        
       
    }
});

require([
    'apps/app'
], function (
    App
) {
    App.initialize();
});