define([
        'backbone',
        'marionette',
        'appEvents',
        'recipeItemView'

],
    function (
        Backbone,
        Marionette,
        AppEvents,
        RecipeItemView


        ) {
        var RecipesCollectionView = Backbone.Marionette.CollectionView.extend({
            itemView: RecipeItemView,
            tagName: 'ul',
            
            
            regions: {

            },

            events:
            {
               

            },

            initialize: function (options) {
                _.bindAll(this);
                
            },

            onRender: function ()
            {


            },

            onClose: function () {


            },

            

        });

        return RecipesCollectionView;
    });