define([
        'backbone',
        'marionette',
        'text!../templates/recipe-template.html',
        'appEvents',
        


],
    function (
        Backbone,
        Marionette,
        RecipeTemplate,
        AppEvents
        


        ) {
        var RecipeView = Backbone.Marionette.Layout.extend({
            id:'recipe-view',

            template: _.template(RecipeTemplate),

            regions: {
               
            },

            events:
            {
               'click #back-btn':'handleBackClick'
            },

            initialize: function () {
                _.bindAll(this);

            },

            onRender: function () {
              

            },

            onClose: function () {
              
            },

            handleBackClick: function (e) {
                Backbone.history.navigate('', true)
            },


        });

        return RecipeView;
    });