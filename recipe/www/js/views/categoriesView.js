define([
        'backbone',
        'marionette',
        'appEvents',
        'categoryItemView'

],
    function (
        Backbone,
        Marionette,
        AppEvents,
        CategoryItemView


        ) {
        var CategoriesView = Backbone.Marionette.CollectionView.extend({
            itemView: CategoryItemView,
           
            recipes: null,
            categories:null,
            
            regions: {

            },

            events:
            {
               

            },

            initialize: function (options) {
                _.bindAll(this);
                //this.itemViewOptions(this.model,0)
               // this.recipes = options.recipes;
                //this.categories = options.categories;
                //console.log("m", this.model)
                //this.collection = this.model.get('categories')
                console.log('colview', this.itemView)
            },

            
           // buildItemView: function (item, ItemViewType, itemViewOptions)
            //{

                //console.log("ms", this.model)
                // build the final list of options for the item view type
                //var options = _.extend({ model: item });
                 //create the item view instance
                //var view = new ItemViewType({ model: item });
                // return it
               // console.log('build', item)
              //return view;
            //},

            onRender: function ()
            {


            },

            onClose: function () {


            },

            

        });

        return CategoriesView;
    });