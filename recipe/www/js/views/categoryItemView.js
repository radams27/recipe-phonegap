define([
        'backbone',
        'marionette',
        'text!../templates/node-template.html',
        'appEvents',
        'tweenlite'
        //'recipesCollectionView'

],
    function (
        Backbone,
        Marionette,
        CategoryItemTemplate,
        AppEvents,
        TweenLite
        //RecipesCollectionView


        ) {
        var CategoryItemView = Backbone.Marionette.ItemView.extend({
            tagName:'ul',
            className:'recipe-list',
            template: _.template(CategoryItemTemplate),
            touchDelay: null,
            endHold:null,
            itemRegion:null,
            isOpen: null,
            isAddRecipe:null,

            regions: {
                //recipesList:'.recipes-list'
            },

            events:{

               // 'mousedown ': 'handleMouseDown',
                //'mouseup ': 'handleMouseUp',
                'click .test-btn': 'handleSelect',
                'click .remove-item': 'handleRemoveRecipe'
               
                //'click .recipe-list li': 'handleRecipeItemClick'
            },

            triggers:
            {
              

            },

            initialize: function (options) {
                _.bindAll(this);
              
            },

            onRender: function ()
            {
               
               
               //var that = this
               //  if(this.model.get('recipes') != undefined){
               //    // console.log('models',this.model.get('recipes').models)
               //     this.$el.html(that.template({ items: that.model.get('recipes').models, category:that.model.get('category') }));
               // }
                //this.$('.item-menu').hide();
                //this.$el.html(that.template());
                //this.append('<div class="item-menu"><div class="remove-item">-</div></div>');
                this.$('li').append('<div class="item-menu"><div class="remove-item">-</div></div>');
                console.log('render item')
            },

            handleSelect:function(e)
            {
                console.log('fdf', this.model)
                //AppEvents.trigger('recipe:show')
                Backbone.history.navigate('/recipe/' + String(this.model.get('cat')) + "/" + String(this.model.get('id')), true)

               AppEvents.trigger('recipe-item:clicked')
            },

            handleRemoveRecipe: function (e) {

                var currentRecipe = $(e.target).parent().parent().find('.tree-item')[0].textContent;
                //console.log('cr ', currentRecipe)
                AppEvents.trigger('recipe-item:remove', currentRecipe);
            },

            handleRecipeItemClick:function(e)
            {
                console.log(e)
            },

            onClose: function () {


            },

            

        });

        return CategoryItemView;
    });