define([
	'jquery',
    'backbone',
    'marionette',
    'tweenlite',
    'appEvents'

],
    function (
	    $,
        Backbone,
        Marionette,
        TweenLite,
        AppEvents
	   // FacebookUserModel
        ) {
        var TransitionRegion = Backbone.Marionette.Region.extend({
            time:null,
  
            initialize:function()
            {
                
                _.bindAll(this);
            },

		    transitionToView: function(newView, isBack) {

			   
		        var self = this;
			    // Do we have a view currently?
			    var view = this.currentView;
			    if (!view || view.isClosed) {
			    this.show(newView);
				    return;
			    }

			   // AppEvents.trigger('pageRegion:transition:inProgress', true);

			    // Wait for the new view to render, then initialize a transition to
			    // show the new view while hiding the old.
			    newView.on('render', function() {
			        //newView.$el.css(self.transformPropName, translation);
			       
			     
			        TweenLite.to(newView.$el, 0, { x: isBack ? -view.$el.width() : view.$el.width(), z: 0.01 });
			        TweenLite.to(newView.$el, .65, { x: 0,z:0.1, clearProps: 'transform',ease:Expo.easeOut, delay:  0 });
			        console.log('w', view.$el.width())
				    // Add the new view to the dom
			    self.$el.append(newView.el);
			  
				    var target_x = -1900;
				   
				    TweenLite.to(view.$el, .65, {
					    alpha: 1,
					    x: isBack ? view.$el.width() : -view.$el.width(),
					    z: 0.1,
					    clearProps: 'transform',
					    ease: Expo.easeOut,
					    onComplete: function() {
						    self.close();
						    self.currentView = newView;

						   // AppEvents.trigger('pageRegion:transition:inProgress', false);
					    }
				    });

			    });

			   // FacebookUserModel.scrollToTop();
              
			    newView.render();

		    },// end transitionToView,

		    transitionToRight: function (newView) {


		        var self = this;
		        // Do we have a view currently?
		        var view = this.currentView;
		        if (!view || view.isClosed) {
		            this.show(newView);
		            return;
		        }

		       // AppEvents.trigger('pageRegion:transition:inProgress', true);

		        // Wait for the new view to render, then initialize a transition to
		        // show the new view while hiding the old.
		        newView.on('render', function () {
		          
		            // Add the new view to the dom
		          
                    var time = 0.5

		            self.$el.prepend(newView.el);
		            $(newView.el).css('left','-685px')
		            TweenLite.to(newView.el, time, { alpha: 1, left: 0, clearProps: 'transform', delay: 0 });

		            
		           // $(newView.el).prependTo(self.$el)
		             //$(this).append(newView.el);

		            var target_x = 685;
		            //if (Utils.is_safari === true) target_x = 0;

		            TweenLite.to(view.el, 0.3, {
		                alpha: 1,
		                left: target_x,
		                clearProps: 'transform',
		                //ease: Expo.easeOut,
		                onComplete: function () {
		                    self.close();
		                    self.currentView = newView;

		                    //AppEvents.trigger('pageRegion:transition:inProgress', false);
		                }
		            });

		        });

		        // FacebookUserModel.scrollToTop();

		        newView.render();

		    },

		    transitionToLeft: function (newView) {


		        var self = this;
		        // Do we have a view currently?
		        var view = this.currentView;
		        if (!view || view.isClosed) {
		            this.show(newView);
		            return;
		        }

		        AppEvents.trigger('pageRegion:transition:inProgress', true);

		        // Wait for the new view to render, then initialize a transition to
		        // show the new view while hiding the old.
		        newView.on('render', function () {
		           
		            //TweenLite.to(newView.$el, 0, { alpha: 1, left: '700px', clearProps: 'transform',  delay: Utils.is_safari ? 0 : .3 });
		            
		            var time = 0.5;

		            $(newView.$el).css('left', '685px');
		            TweenLite.to(newView.$el, time, { alpha: 1, left: 0, clearProps: 'transform', delay: Utils.is_safari ? 0 : 0 });
		            // Add the new view to the dom
		           
		            self.$el.append(newView.el);

		            var target_x = -680;
		           // if (Utils.is_safari === true) target_x = 0;

		            TweenLite.to(view.el, Utils.is_safari ? time : time, {
		                alpha: 1,
		                left: target_x + 'px',
		                clearProps: 'transform',
		                //ease: Expo.easeOut,
		                onComplete: function () {
		                    self.close();
		                    self.currentView = newView;

		                    AppEvents.trigger('pageRegion:transition:inProgress', false);
		                }
		            });

		        });

		        // FacebookUserModel.scrollToTop();

		        newView.render();

		    }


	    });
        return TransitionRegion;
    });
