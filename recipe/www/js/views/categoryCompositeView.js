define([
        'backbone',
        'marionette',
        'appEvents',
        'categoryItemView',
        'text!../templates/node-template.html',

],
    function (
        Backbone,
        Marionette,
        AppEvents,
        CategoryItemView,
        NodeTemplate


        ) {
        var CategoryCompositeView = Backbone.Marionette.CompositeView.extend({
            itemView: CategoryItemView,
            template: _.template(NodeTemplate),
            className: 'category-list',
            tagName: 'ul',
            recipes: null,
            categories: null,
            isOpen: null,
            isAddRecipe: null,
            touchDelay: null,
            recipeItemActive:null,

            regions: {

            },

            events:
            {
                'click .add-item': 'handleAddClick',
                'click .remove-item': 'handleRemoveClick',
                'click .test-btn': 'handleCatClick',
                'mousedown span.tree-item': 'handleMouseDown',
                'mouseup span.tree-item': 'handleMouseUp',
                'touchstart span.tree-item': 'handleMouseDown',
                'touchend span.tree-item': 'handleMouseUp'

            },

            initialize: function (options) {
                _.bindAll(this);
                //console.log('com:', this);
                //if (this.model != undefined && this.model.has('recipe')) {
                //    console.log('col com: ', this.model)
                this.collection = this.model.get('recipes');
                this.isOpen = false;
                this.isAddRecipe = false;
                this.recipeItemActive = false;
                AppEvents.on('recipe-item:remove', this.handleRemoveRecipe);

                //} else {
               // this.collection = new Backbone.Collection({ title: 'adfdsf' }, {title:'gfdg'});
                //}
                //console.log("dffgd", this)
            },

            appendHtml: function (collectionView, itemView) {
                // ensure we nest the child list inside of 
                // the current list item
                if (itemView.model.get('category') == 'default') {
                    collectionView.$("li:first").append('<div class="item-menu"><div class="add-item">+</div><div class="remove-item">-</div></div>');
                    
                    //this.$('.item-menu').hide();
                    //return
                } else {
                    collectionView.$("li:first").append(itemView.el);
                   
                   // this.$('.item-menu').hide();
                }
                   
               
                
            },

            handleAddClick: function (e) {
                //console.log($(e.target).parent().find('.tree-item')[0].textContent);
                // if (!this.isAddRecipe)
                //{
                e.preventDefault();
                var currentCategory = $(e.target).parent().parent().find('.tree-item')[0].textContent;
                AppEvents.trigger('recipe:create-new', currentCategory)
                //}

            },

            handleRemoveClick: function (e) {
                //console.log($(e.target).parent().find('.tree-item')[0].textContent);
                // if (!this.isAddRecipe)
                //{
                e.preventDefault();
                var currentCategory = $(e.target).parent().parent().find('.tree-item')[0].textContent;
                AppEvents.trigger('recipe:remove', currentCategory)
                //}

            },

            setRecipeItemActive: function ()
            {
                this.recipeItemActive = true;
                console.log('setting recipe item')
            },

            handleCatClick: function (e)
            {
                //e.preventDefault();
                //if (this.isAddRecipe == true)
                //{
                //    this.isAddRecipe = false;
                //    return;
                //}

                if (this.recipeItemActive == true)
                    return
                console.log('cat click>>', e)
                if (this.isOpen == false)
                    this.isOpen = true;
                else
                    this.isOpen = false;

                if (this.isOpen) {
                    if ($(e.currentTarget).parent().find('.recipe-list li').length > 0) {
                        var numItems = $(e.currentTarget).parent().find('.recipe-list li').length;
                        var tweenHeight = parseInt(numItems * 111) + 112
                        //console.log('t', this)
                        TweenLite.to(this.$el, .3, { 'height': tweenHeight + 'px',z:1,ease:Expo.easeOut })
                        //$(e.currentTarget).parent().css('height', tweenHeight + 'px')
                        //console.log($(e.currentTarget).parent().find('.recipe-list li'))
                    } else {
                        this.isOpen = false;
                    }
                }
                else {
                    TweenLite.to(this.$el, .3, { 'height': '112px', z: 0.1, ease: Expo.easeOut })
                    //$(e.currentTarget).parent().css('height','40px')
                }
            },

            handleRemoveRecipe: function (e)
            {
                TweenLite.to(this.$el, 0, { 'height': (this.$el.height() - 111) + 'px', z: 0.1, ease: Expo.easeOut })

            },

            handleMouseDown: function (e) {
                // e.preventDefault();
                //var that = this
                //this.touchDelay = setTimeout(function () {
                //    that.$('.item-menu').show();
                //    console.log('item')
                //    that.isAddRecipe = true;
                //}, 500)
            },

            handleMouseUp: function (e) {
                //clearTimeout(this.touchDelay);
            },

            onRender: function () {
                AppEvents.on('recipe-item:clicked', this.setRecipeItemActive)
                //nsole.log('render', this)
            },

            onClose: function () {


            },



        });

        return CategoryCompositeView;
    });