define([
        'backbone',
        'marionette',
        'text!../templates/create-recipe-template.html',
        'appEvents',
        'categoriesView',
        'categoryPopupView',
        'categoryView',
        'tweenlite'


],
    function (
        Backbone,
        Marionette,
        CreateRecipeTemplate,
        AppEvents,
        CategoriesView,
        CategoryPopupView,
        CategoryView,
        TweenLite


        ) {
        var CreateRecipeView = Backbone.Marionette.Layout.extend({
            categoryPopup: null,
            categoriesView: null,
            currentRecipe:null,

            template: _.template(CreateRecipeTemplate),

            regions: {
                popup: '#popup-container',
                categoryList: '#category-list'
            },

            events:
            {
                'click .cancel-btn': 'handleCancelClick',
                'click .save-btn': 'handleSaveClick'

            },

            initialize: function (options) {
                _.bindAll(this);
                this.currentRecipe = options.category;

            },

            onRender: function () {
              
                //TweenLite.from(this.$el, .3, {alpha:0})
            },

            onClose: function () {
                
            },

            handleCancelClick: function (e) {
                
                this.close();
            },

            handleSaveClick: function (e)
            {
                var recipeTitle = this.$('#recipe-title-input')[0].value;
                var recipeContent = this.$('#recipe-content-input')[0].value;

                this.model.addRecipe(recipeTitle, recipeContent, this.currentRecipe);
                this.close();
                AppEvents.trigger('recipe:add-new')
            }

          

        });

        return CreateRecipeView;
    });