define([
        'backbone',
        'marionette',
        'text!../templates/category-popup-template.html',
        'appEvents',
        'tweenlite'

],
    function (
        Backbone,
        Marionette,
        CategoryPopupTemplate,
        AppEvents,
        TweenLite


        ) {
        var CategoryPopupView = Backbone.Marionette.Layout.extend({
            
            template: _.template(CategoryPopupTemplate),
            regions: {

            },

            events:
            {
                'click .cancel-btn': 'handleCancelClick',
                'click .save-btn': 'handleSaveClick',

            },

            initialize: function () {
                _.bindAll(this);

            },

            onRender: function () {

              //console.log('popup render', this.template())
                //TweenLite.from(this.$el, .3, { alpha: 0 })
            },
         

            onClose: function () {

            },

            handleCancelClick: function (e) {
                AppEvents.trigger('popup:cancel')
            },

            handleSaveClick: function (e) {
              
                var inputText = this.$('#category-input')[0].value
                AppEvents.trigger('popup:save', inputText);
            }



        });

        return CategoryPopupView;
    });