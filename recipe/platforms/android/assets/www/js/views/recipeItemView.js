define([
        'backbone',
        'marionette',
        'text!../templates/recipe-item-template.html',
        'appEvents'

],
    function (
        Backbone,
        Marionette,
        CategoryItemTemplate,
        AppEvents


        ) {
        var RecipeItemView = Backbone.Marionette.ItemView.extend({
            tagName:'li',
            template: _.template(CategoryItemTemplate),
            touchDelay: null,
            endHold:null,

            regions: {

            },

            events:{

                //'mousedown': 'handleMouseDown',
               // 'mouseup': 'handleMouseUp',
                //'click .item-menu':'handleMenuClick'
            },

            triggers:
            {
              

            },

            initialize: function (options) {
                _.bindAll(this);
               
                console.log('r', options.recipes, 'c', options.categories)
                
            },

            onRender: function ()
            {
                console.log('render', this)
                this.$('.item-menu').hide();
               
            },

            onClose: function () {


            },

            

        });

        return RecipeItemView;
    });