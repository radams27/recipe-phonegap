define([
        'backbone',
        'marionette',
        'appEvents',
        'text!../templates/category-template.html'
        

],
    function (
        Backbone,
        Marionette,
        AppEvents,
        CatTemplate
       


        ) {
        var CategoryView = Backbone.Marionette.Layout.extend({
            template:_.template(CatTemplate), 

            regions: {

            },

            events:
            {


            },

            initialize: function () {
                _.bindAll(this);
               // this.$el.html(this.template({ posters: this.collection.models }));
                //console.log('init', this.collection.models)
                //console.log('render', this.collection.models)
                
            },

            onRender: function ()
            {
                console.log('render')
              
                
            },

            onClose: function () {

                console.log('close')
            },



        });

        return CategoryView;
    });