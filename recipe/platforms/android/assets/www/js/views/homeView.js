define([
        'backbone',
        'marionette',
        'text!../templates/home-template.html',
        'appEvents',
        'categoriesView',
        'categoryPopupView',
        'categoryView',
        'createRecipeView',
        'categoryCompositeView'

        
],
    function (
        Backbone,
        Marionette,
        HomeTemplate,
        AppEvents,
        CategoriesView,
        CategoryPopupView,
        CategoryView,
        CreateRecipeView,
        CategoryCompositeView
        

        ) {
        var HomeView = Backbone.Marionette.Layout.extend({
            id:'home-view',
            categoryPopup: null,
            categoriesView:null,
            currentCategory:null,
          
            template: _.template(HomeTemplate),
           
            regions: {
                popup: '#popup-container',
                categoryList:'#category-list'
            },

            events:
            {
                'click #add-category-btn': 'handleAddCategory'
            },

            initialize: function () {
                _.bindAll(this);
                
            },

            onRender: function ()
            {
                this.showCategories();

                AppEvents.on('popup:cancel', this.handlePopupCancel);
                AppEvents.on('popup:save', this.handlePopupSave);
                AppEvents.on('recipe:create-new', this.handleNewRecipe);
                AppEvents.on('recipe:add-new', this.handleAddRecipe);
                AppEvents.on('recipe:remove', this.handleRemoveCategory)
                AppEvents.on('recipe-item:remove', this.handleRemoveRecipe);
             
            },
           
            onClose: function ()
            {
                AppEvents.off('popup:cancel', this.handlePopupCancel);
                AppEvents.off('popup:save', this.handlePopupSave);
                AppEvents.off('recipe:create-new', this.handleNewRecipe);
                AppEvents.off('recipe:add-new', this.handleAddRecipe);
                AppEvents.off('recipe:remove', this.handleRemoveCategory);
                AppEvents.off('recipe-item:remove', this.handleRemoveRecipe);
            },

            handleAddCategory: function (e)
            {
                
                this.popup.show(new CategoryPopupView())
            },

            handlePopupCancel: function (e)
            {
                this.popup.close();
             
            },

            showCategories: function ()
            {
                var categoriesView = new CategoriesView({
                    itemView: CategoryCompositeView,
                    collection: this.model.get('categories')
                })
                //var categoryCompositeView = new CategoryCompositeView()
                this.categoryList.show(categoriesView);
            },

            handlePopupSave: function (e)
            {
                this.model.addCategory(e);
                this.popup.close();
                var that = this;

                
                this.showCategories();
                //this.categoriesView = new CategoriesView({ collection: this.model.get('categories') })
                //this.categoryList.show(this.categoriesView);
               // _.each(this.model.get('categories'), function (category) {


               // });

                //console.log(categoriesView)
            },

            handleNewRecipe: function (currentCategory)
            {
                this.popup.show(new CreateRecipeView({ model: this.model, category: currentCategory }));
                //console.log(this.model.get('recipes'))
            },

            handleAddRecipe: function (e)
            {
                //update list with recipe
                //this.categoriesView = new CategoriesView({ collection: this.model.get('categories')})
                //this.categoryList.show(this.categoriesView);
                console.log('add recipe')
            },
            handleRemoveCategory: function (e)
            {
                this.model.deleteCategory(e)

            },

            handleRemoveRecipe: function (e)
            {
                this.model.deleteRecipe(e)
            }


        });

        return HomeView;
    });