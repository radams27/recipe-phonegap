﻿define([
        'underscore',
        'backbone',
         
        'marionette'
],
    function (
        _,
        Backbone,
        Marionette
    ) {
       var AppEvents = _.extend({}, Backbone.Events);
        //var AppEvents = new Marionette.EventAggregator();

        return AppEvents;
    });